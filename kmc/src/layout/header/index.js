import { useNavigate } from "react-router-dom"
import { Auth, HeaderItem } from "../../utlis/Items"

const Index=()=>{
    const navigate=useNavigate()
    const handelClick=(e)=>{
     console.log('dataa',e)
     navigate(e)
    }
    return(
        <div style={{display:'flex',justifyContent:'space-between'}} className='!text-white'>
            <div className='!text-white'>
                Logo
            </div>
            <div style={{display:'flex',gap:"4px"}}>
            {
                HeaderItem?.map((item)=>(
                    <div key={item.link}  onClick={()=>handelClick(item.link)}>
                        {item.name}
                    </div>
                ))
            }
        </div>
        <div  style={{display:'flex',gap:"4px"}}>
            {
                Auth?.map((item)=>(
                    <div key={item.link} style={{color:'white'}} onClick={()=>handelClick(item.link)}>
                        {item.name}
                    </div>
                ))
            }
        </div>
        </div>
        
    )
}
export default Index