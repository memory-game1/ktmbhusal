import { Outlet } from "react-router-dom"
import MainHeaders from './header'
import { Content, Footer, Header } from "antd/es/layout/layout"
import { Layout, theme } from "antd";


const UserLayout=()=>{
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
    return(
      <div>
      <Layout>
     <Header>
     <MainHeaders/>
     </Header>
     <Content
       style={{
         padding: '0 48px',
       }}
     >
     
       <Layout
         style={{
           padding: '24px 0',
           background: colorBgContainer,
           borderRadius: borderRadiusLG,
         }}
       >
        
         <Content
           style={{
             padding: '0 24px',
             minHeight: 280,
           }}
         >
          <div>
          <Outlet/>

          </div>
         </Content>
       </Layout>
     </Content>
     <Footer
       
     >
      This is footer
     </Footer>
   </Layout>
  
   </div>
    )
}
export default UserLayout