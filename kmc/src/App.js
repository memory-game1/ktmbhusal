import { Button, Checkbox, Form, Input } from "antd"

const FormSubmit=()=>{
  const onFinish=(values)=>{
   console.log('values',values)
  }

  return(
    <div>
    <Form onFinish={onFinish} layout="vertical" >
      <Form.Item 
      name={"email"} 
      label="Email"
      rules={[
        {
          type: "email", 
          message: 'The input is not a valid phone number!',
        },
        {
          required: true,
          message: 'Please input your phone number!',
        },
      ]}
      // hasFeedback
      >
        <Input/>
      </Form.Item>
      <Form.Item name={"last_name"} label="Last Name ">
        <Input />
      </Form.Item>

      <Form.Item 
      name={'email'} 
      label="Email" 
      
      >
        <Input  type="number" />
      </Form.Item>
      <Form.Item
      name="remember"
      valuePropName='checked'
   
    >
      <Checkbox>Remember me</Checkbox>
    </Form.Item>
    <Form.Item 
    name={"contact"} 
    label="Contact" 
    rules={[
      {
        pattern: /^\d{10}$/, // Assuming a 10-digit phone number, adjust as needed
        message: 'The input is not a valid phone number!',
      },
      {
        required: true,
        message: 'Please input your phone number!',
      },
    ]}
    >
     <Input type="number"/>
    </Form.Item>
      <Form.Item name={"description"} label="Description">
        <Input.TextArea/>
      </Form.Item>
      <Button htmlType="submit">
        Form Submit
      </Button>
    </Form>
    </div>
  )
}
export default FormSubmit