import React from 'react'
import UserItems from './UserItems'
import { HotproductData } from '../../utlis/Items'

const HotProducts = () => {
  return (
    <div>
        <UserItems data={HotproductData} title="Hot Products"/>
    </div>
  )
}

export default HotProducts