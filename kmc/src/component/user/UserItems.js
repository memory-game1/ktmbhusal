import { Card } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";


const UserItems = ({ data, title }) => {

  const navigate=useNavigate()

  const handelClick=(item)=>{
    navigate(`/userdetail/${item.id}`)
   
    localStorage.setItem('userdetail', JSON.stringify(item));
  }
  return (
    <div>
      <div className='!text-[red]'>{title}</div>
      <div style={{display:'flex' ,justifyContent:'flex-start',gap:'10px'}}>
        {data?.map((item) => (
          <div key={item.id} onClick={()=>handelClick(item)}>
            <Card
              hoverable
              style={{
                width: 240,
              }}
              cover={
                <img
                  alt="example"
                  src={item.image}
                />
              }
            >
             <div>
                Price :{item.price}
             </div>
             <div>
                Brand : {item.brand}
             </div>
            </Card>
          </div>
        ))}
      </div>
    </div>
  );
};

export default UserItems;
