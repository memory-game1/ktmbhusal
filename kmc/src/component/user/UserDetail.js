import { EyeOutlined } from "@ant-design/icons";
import { Card, Rate } from "antd";
import React from "react";


const UserDetail = () => {
 
  const myValue = localStorage.getItem("userdetail");
  const data = JSON.parse(myValue);
 
  console.log("userdetail", JSON.parse(myValue));
  return (
    <div style={{display:'flex' ,justifyItems:'center',gap:'10px'}}>
      <div>
        <Card
          hoverable
          style={{
            width: 440,
          }}
          cover={<img alt="example" src={data?.image} />}
        />
      </div>
      <div>
        <div>
          {data?.name}
        </div>
        <div>
         Rate :<Rate value={data?.rate}/>
        </div>
        <div>
        <EyeOutlined /> {data?.view} people viewed this product
        </div>
      </div>
    </div>
  );
};

export default UserDetail;
