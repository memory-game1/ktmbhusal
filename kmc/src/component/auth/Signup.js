import { Button, Card, Form, Input } from 'antd'
import React from 'react'
import { Link } from 'react-router-dom'

const Signup = () => {
  return (
    <div
      style={{
        width: "500px",
        height: "auto",
        margin: "auto",
        paddingTop: "10rem",
      }}
    >
      <Card>
        <div style={{ display: "flex", justifyContent: "center" }}>Sign Up</div>
        <Form layout="vertical">
          <Form.Item name={"first_name"} label="First Name">
            <Input />
          </Form.Item>
          <Form.Item name={"last_name"} label="Last Name">
            <Input />
          </Form.Item>
          <Form.Item name={"contact"} label="Contact">
            <Input />
          </Form.Item>
          <Form.Item name={"email"} label="Email">
            <Input />
          </Form.Item>
          <Form.Item name={"password"} label="Password">
            <Input.Password />
          </Form.Item>
          <Form.Item name={"confirm_password"} label="Confirm Password">
            <Input.Password />
          </Form.Item>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div>
              <Button>Signup</Button>
            </div>
            <div>
              <Link to="/auth/login">Already have an account? Login</Link>
            </div>
          </div>
        </Form>
      </Card>
    </div>
  )
}

export default Signup