import { Button, Card, Form, Input } from "antd";
import React from "react";
import { Link } from "react-router-dom";

const Login = () => {
  return (
    <div
      style={{
        width: "500px",
        height: "auto",
        margin: "auto",
        paddingTop: "10rem",
      }}
    >
      <Card>
        <div style={{ display: "flex", justifyContent: "center" }}>Login</div>
        <Form layout="vertical">
          <Form.Item name={"user_name"} label="User Name">
            <Input />
          </Form.Item>
          <Form.Item name={"password"} label="Password">
            <Input.Password />
          </Form.Item>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div>
              <Button>Login</Button>
            </div>
            <div>
              <Link to="/auth/signup">Don't have an account? Sign Up</Link>
            </div>
          </div>
        </Form>
      </Card>
    </div>
  );
};

export default Login;
