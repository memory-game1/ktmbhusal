import {
    Route,
    createBrowserRouter,
    createRoutesFromElements,
  } from "react-router-dom";
import AdminLayout from "../layout/AdminLayout";
import AdminDashBoard from "../component/admin/DashBoard";
import UserDashBoard from "../component/user/DashBoard";
import UserLayout from "../layout/UserLayout";
import AboutUs from "../component/aboutUs/AboutUs";
import PageNotFound from "../component/PageNotFound";
import UserDetail from "../component/user/UserDetail";
import AuthLayout from "../layout/AuthLayout";
import Login from "../component/auth/Login";
import Signup from "../component/auth/Signup";
import HotProducts from "../component/user/HotProducts";
import TrendingVendors from "../component/user/TrendingVendors";

  export const Routers = createBrowserRouter(
    createRoutesFromElements(
      <Route>

        <Route path="/" element={<UserLayout/>}>
           <Route index element={<UserDashBoard/>}/>
           <Route path="aboutus" element={<AboutUs/>}/>
           <Route path="userdetail/:id" element={<UserDetail/>}/>
           <Route path="hotproducts" element={<HotProducts/>}/>
           <Route path="ternding/vendors" element={<TrendingVendors/>}/>

          
        </Route>

        <Route path="/admin" element={<AdminLayout/>}>
           <Route index element={<AdminDashBoard/>}/>
        </Route>

        <Route path="auth" element={<AuthLayout/>}>
           <Route path="login" element={<Login/>}/>
           <Route path="signup" element={<Signup/>}/>

        </Route>

        <Route path="*" element={<PageNotFound/>}/>
        

      </Route>
    )
  );
 